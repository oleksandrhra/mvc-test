<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:43
 */

class Controller_new_task extends Controller
{
    function __construct()
    {
        $this->model = new Model_new_task();
        $this->view = new View();
    }

    function action_download()
    {
       $this->model->insert_data($_POST['name'], $_POST['email'], $_POST['comment']);


        $this->model->get_data();
        $this->view->generate('new_task_view.php', 'template_view.php');

    }

    function action_index()
    {
        $this->view->generate('new_task_view.php', 'template_view.php');
    }

}
?>