<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:42
 */

class Model_new_task extends Model
{

    function insert_data($a1,$a2,$a3){
        include('connect.php');
            $connect = connectPDO();
            $statement = $connect->prepare('INSERT INTO tasks(id,name_c,e_mail,text_task,img_path,status) VALUES (:id,:name_c,:e_mail,:text_task,:img_path,:status)');
            $statement ->bindParam(':id', $id);
            $statement ->bindParam(':name_c', $name_c);
            $statement ->bindParam(':e_mail', $e_mail);
            $statement ->bindParam(':text_task', $text_task);
            $statement ->bindParam(':img_path', $img_path);
            $statement ->bindParam(':status', $status);
            $id = 0;
            $name_c = $a1;
            $e_mail = $a2;
            $text_task = $a3;
            $img_path = "http://sketch.pp.ua/images/task/1489600348.jpg";
            $status = "no";
            $statement->execute();

    }

    function get_data()
    {


        // Пути загрузки файлов
        $path = "images/";
        $tmp_path = "images/";
// Массив допустимых значений типа файла
        $types = array('image/jpg', 'image/gif', 'image/png');
// Максимальный размер файла
        $size = 1024000;

// Обработка запроса
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Проверяем тип файла
            if (!in_array($_FILES['picture']['type'], $types))
                return('<p>Запрещённый тип файла. <a href="?">Попробовать другой файл?</a></p>');

            // Проверяем размер файла
            if ($_FILES['picture']['size'] > $size)
                return('<p>Слишком большой размер файла. <a href="?">Попробовать другой файл?</a></p>');

            // Функция изменения размера
            // Изменяет размер изображения в зависимости от type:
            //	type = 1 - эскиз
            // 	type = 2 - большое изображение
            //	rotate - поворот на количество градусов (желательно использовать значение 90, 180, 270)
            //	quality - качество изображения (по умолчанию 75%)
            function resize($file, $type = 1, $rotate = null, $quality = null)
            {
                global $tmp_path;

                // Ограничение по ширине в пикселях
                $max_thumb_size = 320;
                $max_size = 240;

                // Качество изображения по умолчанию
                if ($quality == null)
                    $quality = 75;

                // Cоздаём исходное изображение на основе исходного файла
                if ($file['type'] == 'image/jpeg')
                    $source = imagecreatefromjpeg($file['tmp_name']);
                elseif ($file['type'] == 'image/png')
                    $source = imagecreatefrompng($file['tmp_name']);
                elseif ($file['type'] == 'image/gif')
                    $source = imagecreatefromgif($file['tmp_name']);
                else
                    return false;

                // Поворачиваем изображение
                if ($rotate != null)
                    $src = imagerotate($source, $rotate, 0);
                else
                    $src = $source;

                // Определяем ширину и высоту изображения
                $w_src = imagesx($src);
                $h_src = imagesy($src);

                // В зависимости от типа (эскиз или большое изображение) устанавливаем ограничение по ширине.
                if ($type == 1)
                    $w = $max_thumb_size;
                elseif ($type == 2)
                    $w = $max_size;

                // Если ширина больше заданной
                if ($w_src > $w) {
                    // Вычисление пропорций
                    $ratio = $w_src / $w;
                    $w_dest = round($w_src / $ratio);
                    $h_dest = round($h_src / $ratio);

                    // Создаём пустую картинку
                    $dest = imagecreatetruecolor($w_dest, $h_dest);

                    // Копируем старое изображение в новое с изменением параметров
                    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

                    // Вывод картинки и очистка памяти
                    imagejpeg($dest, $tmp_path . $file['name'], $quality);
                    imagedestroy($dest);
                    imagedestroy($src);

                    return $file['name'];
                } else {
                    // Вывод картинки и очистка памяти
                    imagejpeg($src, $tmp_path . $file['name'], $quality);
                    imagedestroy($src);

                    return $file['name'];
                }
            }

            $name = resize($_FILES['picture'], $_POST['file_type'], $_POST['file_rotate']);

            // Загрузка файла и вывод сообщения
            if (!@copy($tmp_path . $name, $path . $name))
                return '<p>Что-то пошло не так.</p>';
            else
                return '<p>Загрузка прошла удачно <a href="' . $path . $_FILES['picture']['name'] . '">Посмотреть</a>.</p>';

            // Удаляем временный файл
            unlink($tmp_path . $name);
        }
    }
}

?>