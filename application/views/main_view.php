<div class="main"><!-- start main -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="container">
                <div class="features"><!-- start feature -->
                    <div class="new_text">
                        <div class="col-lg-11 ">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="main">Приложение-задачник:</a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="main">Главная</a></li>
                                <li><a href="new_task/index">Добавить новую задачу</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-1 ">
                            <ul class="nav navbar-nav">
                                <li><a href="admin.php">admin</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="main" ><!-- start main -->
    <div class="container" style="background: azure">
        <div class="features"><!-- start feature -->
            <div class="new_text">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr><th></th>
                                <th class="field_to_order"  id="username"><a href="main/sort_name">Имя</a><span  class="up glyphicon"></span></th>
                                <th class="field_to_order" id="email"><a href="main/sort_mail">е-mail</a>><span class="up glyphicon"></span></th>
                                <th>Задача</th>
                                <th class="field_to_order" id="status"><a href="main/sor_status">Статус</a>><span class="up glyphicon"></span></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody id="selection">

                            <?php
                            echo $data;
                            ?>

                            <tr>
                                <!--th scope="row">21</th-->
                                <td><a href="/task/overview/21"><img class="img_sm img-rounded" src="http://sketch.pp.ua/images/task/1489600348.jpg"></a></td>
                                <td>Петренко</td>
                                <td>petro@ww.com</td>
                                <td class="task_body_row">Откуда он появился?<br>
                                    Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. <br>
                                    <br>
                                    Р ...</td>
                                <td class="task_status_row">Просрочено</td>
                                <!--td>
                                              <a class="btn btn-default btn-sm" href="/task/edit/21" role="button">Изменить</a>
                                            </td-->
                                <td>
                                    <a class="btn btn-warning btn-sm" href="/task/edit/21" role="button">Изменить</a><br><br>
                                    <a class="btn btn-default btn-sm" href="/task/overview/21" role="button">Просмотр</a>
                                </td>
                            </tr>

                            <tr>
                                <!--th scope="row">21</th-->
                                <td><a href="/task/overview/21"><img class="img_sm img-rounded" src="http://sketch.pp.ua/images/task/1489600348.jpg"></a></td>
                                <td>Петренко</td>
                                <td>petro@ww.com</td>
                                <td class="task_body_row">Откуда он появился?<br>
                                    Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. <br>
                                    <br>
                                    Р ...</td>
                                <td class="task_status_row">Просрочено</td>
                                <!--td>
                                              <a class="btn btn-default btn-sm" href="/task/overview/21" role="button">Детальнее</a>
                                            </td-->
                                <td>
                                    <a class="btn btn-default btn-sm" href="/task/overview/21" role="button">Просмотр</a>
                                </td>
                            </tr>

                            <tr>
                                <!--th scope="row">29</th-->
                                <td><a href="/task/overview/29"><img class="img_sm img-rounded" src="http://sketch.pp.ua/images/task/1489701614.jpg"></a></td>
                                <td>Данило</td>
                                <td>dan@dd.com</td>
                                <td class="task_body_row">xX ...</td>
                                <td class="task_status_row">Выполнено</td>
                                <!--td>
                                              <a class="btn btn-default btn-sm" href="/task/overview/29" role="button">Детальнее</a>
                                            </td-->
                                <td>
                                    <a class="btn btn-default btn-sm" href="/task/overview/29" role="button">Просмотр</a>
                                </td>
                            </tr>

                            <tr>
                                <!--th scope="row">29</th-->
                                <td><a href="/task/overview/29"><img class="img_sm img-rounded" src="http://sketch.pp.ua/images/task/1489701614.jpg"></a></td>
                                <td>Данило</td>
                                <td>dan@dd.com</td>
                                <td class="task_body_row">xX ...</td>
                                <td class="task_status_row">Выполнено</td>
                                <!--td>
                                              <a class="btn btn-default btn-sm" href="/task/overview/29" role="button">Детальнее</a>
                                            </td-->
                                <td>
                                    <a class="btn btn-default btn-sm" href="/task/overview/29" role="button">Просмотр</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div></div></div></div></div>
<br>