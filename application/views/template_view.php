<!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
    <meta charset="utf-8">
    <title>HTML5 Загрузка и обрезка изображений с Jcrop</title>

    <!-- add styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/css/styles.css" />
    <!-- add scripts -->
    <script src="/js/jquery-3.2.1.js" type="text/javascript"></script>
</head>
<body  style="background: skyblue" >

<?php include 'application/views/'.$content_view; ?>

<style>
    html {
        position: relative;
        min-height: 100%;
    }

    body {
        margin-bottom: 60px;// значение высоты #footer
    }
    #footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 60px;
        background-color: #F0F0E9;
    }
    p{text-align:  center;}
</style>
<footer id="footer">
<div class="footer-bottom" style="background: #333">
    <div class="container">
        <div class="row" style="color: #E0E0F3"><br>
            <div class="col-md-12" style="color: aliceblue">
                <p><span>© Тест по программированию:</span></p>
                <p>Разработано Олександром Храбаном</p>
            </div>
        </div>
    </div>
</div>
</footer><!--/Footer-->
</body>
</html>